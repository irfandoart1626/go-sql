package main

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

// connect to db
const (
	host     = "localhost"
	port     = 5432
	user     = "irfando"
	password = "Powerup1836"
	dbname   = "GoSQL"
)

var (
	db  *sql.DB
	err error
)

type Employees struct {
	ID        int
	Full_Name string
	Email     string
	Age       int
	Division  string
}

func main() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	fmt.Println("Succsessfully connected to DB GoSQL")

	// CreateEmployee()
	// GetEmployees()
	// UpdateEmployee()
	DeleteEmployee()

}

func CreateEmployee() {
	var employees = Employees{}

	sqlStatement := `
	INSERT INTO employees (full_name, email, age, division)
	VALUES ($1, $2, $3, $4)
	Returning *
	`
	err = db.QueryRow(sqlStatement, "Tigor", "gor@gmail.com", 26, "IT").
		Scan(&employees.ID, &employees.Full_Name, &employees.Email, &employees.Age, &employees.Division)

	if err != nil {
		panic(err)
	}

	fmt.Printf("New Employee Data: %+v\n", employees)
}

func GetEmployees() {
	var results = []Employees{}
	sqlStatement := `SELECT * from employees`
	rows, err := db.Query(sqlStatement)

	if err != nil {
		panic(err)
	}

	defer rows.Close()

	for rows.Next() {
		var employees = Employees{}

		err = rows.Scan(&employees.ID, &employees.Full_Name, &employees.Email, &employees.Age, &employees.Division)

		if err != nil {
			panic(err)
		}

		results = append(results, employees)
	}

	fmt.Println("Employee datas:", results)
}

func UpdateEmployee() {
	sqlStatement := `
	UPDATE employees SET full_name = $2, email = $3, division = $4, age = $5
	WHERE id = $1;`

	res, err := db.Exec(sqlStatement, 1, "Jon Cena", "cenjon@gamil.com", "Andalas", 26)

	if err != nil {
		panic(err)
	}
	count, err := res.RowsAffected()
	if err != nil {
		panic(err)
	}
	fmt.Println("Update data amount:", count)
}

func DeleteEmployee() {
	sqlStatement := `
	DELETE from employees
	WHERE id =$1;`

	res, err := db.Exec(sqlStatement, 1)
	if err != nil {
		panic(err)
	}
	count, err := res.RowsAffected()
	if err != nil {
		panic(err)
	}
	fmt.Println("Deleted data amount:", count)
}
